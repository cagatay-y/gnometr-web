---
title: "Bize Katılın"
date: 2018-06-14T01:45:27+03:00
---

## Çevirilere Nasıl Katılırım?

Çeviri yapmak GNOME hakkında bir çok şeyi öğrenmenizi sağlayacak ve Özgür Yazılım için katkıda bulunmanıza olanak sağlayacaktır.

GNOME Türkiye çeviri çalışmalarına isteyen herkes katılabilir. Belirli bir düzeyde ingilizce bilgisine sahipseniz sizlerde çeviri çalışmalarımıza katılabilir, ve özgür yazılım dünyasına katkıda bulunabilirsiniz.

Çeviri yaparken “Çeviri Nasıl Yapılır?” başlıklı yazıda belirtilenler dışında yardıma ihtiyacınız olabilir. Bu yüzden çeviri yapmadan önce Gnome-Turk elektronik posta listesine üye olmanız işinizi kolaylaştıracaktır. Bu sayede diğer çeviri yapanlardan yardım alabilir, etkinlikleri (evet 2-3 ayda bir eğlence ve toplanti amaçlı buluşuyoruz) daha kolay takip edebilirsiniz. Gnome listesine üye olmak için http://mail.gnome.org/mailman/listinfo/gnome-turk adresine gidebilirsiniz.

Gnome-Turk e-posta listesine üye olduktan sonra ve çeviri yapmak istediğinizi çeviri koordinatörlerine (listeye çeviri yapmak istediğinizi belirten bir eposta atmanız yeterli) belirttikten sonra sizlere çeviri işleyişini görebilmeniz için kısa, basit bir çeviri dosyası (po dosyası) örnek olarak verilecek ve bunu çevirmeniz istenecektir.

Çevirlere nasıl katılabileceğiniz ile ilgili detaylı bir açıklamayı [Çeviri Depou İş Akışı](../ceviri/ceviri-deposu-is-akisi) belgesinden edinebilirsiniz.

Gnome Türkiye grubuna şimdiden hoş geldiniz!