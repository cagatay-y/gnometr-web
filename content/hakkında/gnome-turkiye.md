---
title: "GNOME Türkiye"
date: 2018-06-14T01:43:45+03:00
---

GNOME Türkiye, bir grup GNOME sever Linux kullanıcısının 2004 yılı Ekim ayında kurduğu organizasyondur. GNOME Türkiye’nin öncelikli ve temel hedefi GNOME yazılım geliştiricilerini, çevirmenlerini, kullanıcılarını ve uzmanlarını bir araya getirerek sinerji oluşturmak, bu alanda bilgi birikimi yaratarak GNOME kullanımını artırmaktır.

GNOME Türkiye, bu hedefine ulaşmak için,

- Çeşitli seminer, panel ve konferanslarda Linux’un masaüstündeki durumuna ilişkin bilgilendirici çalışmalarda bulunur.
- GNOME sürümlerinin çıkması ile basın bültenleri hazırlar, bunları görsel ve işitsel basına dağıtır.
- Çalışma grupları kurar ve/veya kurulmasına yönelik insiyatif verir.
- Yerelleştirme yaparak GNOME’un her sürümünün Türkçe desteğinin tam olmasını sağlar.
- E-posta listeleri ve web yoluyla bir tartışma ve bilgi alışveriş ortamı oluşturur.

GNOME Türkiye bir dernek veya vakıf statüsünde olmadığı için katılım için e-posta üyeliği tek gerek şarttır. Üyelik için [Nasıl katılabilirim?](../../bize-katilin) bağlantısına girmeniz ve e-posta listesine üye olmanız yeterlidir.

GNOME Türkiye, GNOME kullanan herkesi bu masaüstü ortamını desteklemeye çağırıyor.