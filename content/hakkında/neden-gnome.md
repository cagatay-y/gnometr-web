---
title: "Neden GNOME?"
date: 2018-06-14T01:35:01+03:00
---

GNOME hemen hemen tüm amaçlar için ideal bir seçimdir:

GNOME; IBM, HP, ve Sun gibi lider firmaların seçtiği masaüstüdür. İspanya, Pekin, Çin, ve Amerika Birleşik Devletleri’nde bazı eyaletlerde kamu kurumlarında masaüstü olarak GNOME tercih edilmektedir. Çok sayıda kullanıcı ve şirket sayesinde uzun bir süre destek ve kalite garantisi alabileceğinizden emin olabilirsiniz.
 
## İş için GNOME
Eğer işinizde maliyetleri düşürmek ve güvenilirliği arttırmak istiyorsanız GNOME sizin açık tercihiniz olacaktır. GNOME’un verimli lisanslama modeli sayesinde yüksek lisans paraları ödemek ve lisansları takip etmek için büyük harcamalar yapmak zorunda değilsiniz, ve buna ek olarak şirketinizde ya da evinizde kullanmak için hazırladığınız uygulamalarınıza kaynak kodunu açmasanız dahi telif ücreti ödemek zorunda değilsiniz. Ayrıca GNOME minimum eğitim gerektirir ve destek, yönetim, kurulum, bakım gibi konularda maliyetlerinizi düşürmenizi sağlar.
 
## Ev kullanıcıları için GNOME
GNOME’u öğrenmek ve kullanmak kolaydır: Kullanılabilirlik ekibi bunun sağlanmasından sorumludur.Daha fazla bilgiyi https://developer.gnome.org/hig/stable/ adresinde bulabilirsiniz. GNOME günlük kullanımınız için gerekli tüm yazılımlara sahiptir: oyunlar, tarayıcı, e-posta, ofis uygulamaları ve daha fazlası. Buna ek olarak, Windows kullanıcılarının size gönderdiği dosyalarla çalışabilmenizi sağlayan harika bir Windows dosya uyumluluğuna, çok geniş bir yardım sistemine ve kaynağa sahiptir.

## Geliştiriciler için GNOME
GNOME geliştiricilere en fazla lisans esnekliğini ve programlama dili seçme imkanını sunmaktadır. Buna ilaveten , Windows üzerinde de Unix ve Linux sistemlerde kullandığınız gibi gtk+’yı kullanabilirsiniz.. GNOME kütüphanelerinin birçoğu LGPL şartları altında sunulmaktadır ki bu son ürününüzde kullandığınız lisans ne olursa olsun telif ücreti ödemeyeceğiniz anlamına gelmektedir. GNOME nesne sistemi diğer birçok dile kolayca adapte edilebildiği için Java, C#, Python, Perl ve C/C++ gibi onlarca programlama dili arasından istediğinizi seçme imkanı sunar. http://developer.gnome.org/arch/lang adresinden bu konuda daha fazla bilgi alabilir ve teknik detayları öğrenebilirsiniz.

## Dünya için GNOME
GNOME https://l10n.gnome.org/teams/ adresinde kendi dilinizde mevcut. Birçok yazılım firması genelde küçük diller için çeviri yapmamakta , fakat GNOME Azerice, Arapça, Çince, İbranice, İspanyolca ve İsveççe gibi hem büyük hem de küçük onlarca dil için bunu kolaylaştırmaktadır.

## Engelli insanlar için GNOME
Eğer sınırlı hareket ya da görme kabiliyetine sahipseniz GNOME’un erişilebilir yazılım projesi ile ciddi olarak ilgilendiğini duymaktan mutlu olacağınızı düşünüyoruz. https://wiki.gnome.org/Accessibility adresinden konu ile ilgili daha fazla bilgi alabilir ve projenin gelecek planlarını inceleme fırsatını bulabilirsiniz. GNOME ekran okuyucuları, görme engelliler için kabartma klavyeler, ekran büyütücüler gibi uygulamalar ile aralarında Birleşik Devletler Savunma Bakanlığı gibi kurum ve kuruluşların erişilebilirlik ihtiyaçlarını karşılamaktadır.

## Sistem Yöneticileri için GNOME
Yöneticiler GNOME tercih etmektedir, çünkü kolay kullanım demek kolay destek verebilmek demektir, ve uzak masaüstü ile sistem yönetimi asansörlerde veya bekleme salonlarında harcadığınız zamanı azaltacak bir çözümdür. https://help.gnome.org/admin/ adresinde GNOME Masaüstü Sistem Yönetimi Kılavuzu’nu bulabilir ve masaüstünüzün yönetimi ile her tür bilgiyi edinebilirsiniz.