---
title: "GNOME Nedir?"
date: 2018-06-14T01:19:01+03:00
---

GNOME Projesi iki şey sağlamaktadır: GNOME masaüstü ortamı, kullanıcılar için sezgisel ve çekici bir masaüstü, ve GNOME geliştirme platformu, masaüstüne entegre olabilecek uygulamaların geliştirilmesi için geniş bir altyapı.GNOME’un sizin için neler yapabileceği ve neden GNOME seçmeli konularında daha fazla bilgi almak için http://www.gnome.org/about/why.html adresini ziyaret edebilirsiniz. Bu bağlantının Türkçe çevirisi için [Neden GNOME?](neden-gnome) bölümüne bakabilirsiniz.

## Özgürdür
GNOME, kullanıcılara ve geliştiricilere masaüstleri, programları ve verileri konusunda en son seviyede kontrol imkanını sunan, aynı zamanda da GNU projesinin de bir parçası olan özgür yazılım projesidir.

## Kullanılabilirdir
GNOME kullanılabilirliği özelliklere yüklenmek olarak değil herkesin kullanabileceği yazılımlar ortaya koymak olarak görmektedir. Özgür Yazılım’ın ilk ve tek Kullanıcı Arabirimi Rehberi GNOME’un profesyonel ve gönüllü kullanılabilirlik uzmanları tarafından hazırlanmıştır, ve tüm temel GNOME uygulamaları bu kuralları besimsemektedir. GNOME ve kullanılabilirlik ile ilgili daha fazla bilgi için http://developer.gnome.org/projects/gup/ adresini ziyaret edebilirsiniz.

## Erişilebilirdir
Özgür yazılım engelli kullanıcı ve geliştiricileri de kapsayan tüm kullanıcı ve geliştiricilere açık bir yapıdadır. GNOME Erişilebilirlik API altyapısı yıllarca süren çabanın bir sonucudur, ve GNOME’u Unix platformunda en erişilebilir masaüstü yapmıştır . Daha fazla bilgiyi http://developer.gnome.org/projects/gap/ adresinde bulabilirsiniz.

## Uluslararasıdır
GNOME onlarca dilde geliştirilmekte, belgelendirilmekte ve kullanılmaktadır. GNOME’un farklı dillere çevirilmesinin sağlanması için alt yapı çalışmaları her geçen gün yerelleştirmeyi daha kolay kılmaktadır. Çeviri projesi ile ilgili daha fazla bilgiyi http://developer.gnome.org/projects/gtp/ adresinde bulabilirsiniz. Türkçe yerelleştirme çalışmalarını GNOME Türkiye web sayfasından takip edebilirsiniz.

## Geliştirici dostudur
GNOME ile geliştiriciler bir dile bağımlı kalmamaktadır.C, C++, Python, Perl, Java, hatta C# gibi dilleri kullanarak kolayca Unix veya Linux masaüstünüze entegre olabilecek yüsek kalitede uygulamalar yazabilirsiniz.

## Organizedir
GNOME yüzlerce üyeden, kullanılabilirlik, erişilebilirlik, KG takımları ve seçilmiş bir kuruldan oluşan bir vakıfa sahip organize bir topluluktur. GNOME sürümleri GNOME Sürüm Takım’ı tarafından belirlenir ve her altı ayda bir güncellenir. Konu ile ilgili daha fazla bilgiyi http://www.gnome.org/start/unstable/ adresinde bulabilirsiniz.

## Desteklenmektedir
Dünya çapındaki GNOME topluluğunun yanısıra, GNOME Unix ve Linux iş dünyasındaki HP, Intel, MandrakeSoft, Novell, Red Hat, ve Sun gibi lider firmalar tarafından da desteklenmektedir. Daha fazla bilgiyi http://foundation.gnome.org/advisory_board.html adresinde bulabilirsiniz.

## Bir topluluktur
Belki de her şeyden önce GNOME, dünya çapında geliştirme, çeviri, kalite kontrol gibi faaliyetlere katılan bu beraberlikten keyif alan bir topluluktur. Daha fazla bilgi içi http://developer.gnome.org/helping adresini ziyaret edebilir ve neler yapabileceğinizi görebilirsiniz.