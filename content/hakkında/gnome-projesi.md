---
title: "GNOME Projesi"
date: 2018-06-14T01:24:21+03:00
---

[GNOME projesi](https://www.gnome.org), bütünleşik, serbestçe dağıtılan ve kullanıcı dostu bir arayüz yaratmak için başlatılan bir çalışmadır. GNOME, [GNU](https://www.gnu.org) projesinin bir parçası olduğu için özgür yazılım kapsamına girer. Bu proje, hem kullanıcılar, hem de yazılım geliştiriciler için tam kapsamlı ortam sağlar.

GNOME projesi, iki önemli bileşenden oluşur.

- GNOME masaüstü ortamı:
 
 Kolay kullanılan, gelişmiş ve çekici görsel arayüzüne sahip bir ortam sunar. Özellikle son kullanıcılara yöneliktir.

- GNOME geliştirme ortamı:

 Masaüstündeki programlarla birlikte çalışabilecek uygulamalar yazabilmek için gereken yapıtaşlarını sunar.

 

GNOME projesi, herkesin kullanabileceği kadar kolay masaüstü yaratmayı hedeflemiştir. Bu nedenle profesyonellerden oluşan GNOME topluluğu, ilk [“İnsan Bilgisayar Etkileşimi Kılavuzu”](https://developer.gnome.org/hig/stable/)nu hazırlamıştır. Tüm GNOME uygulamaları bu kılavuzun ortaya koyduğu kurallara uyar.

Türkçe dahil 60’dan fazla dili destekleyen GNOME sayesinde tüm dünyadaki milyonlarca kullanıcı, işletim sistemleriyle kendi dillerinde iletişim kurabilmekte, GNOME sayesinde günlük hayatlarında daha üretken bir şekilde çalışma imkanı sağlamaktadır. [GNOME çeviri projesi](https://wiki.gnome.org/TranslationProject) tüm bu imkanı çeviri grupları için sağlamakta ve yüzlerce çevirmenin GNOME’a destek olmasına olanak vermektedir.

GNOME geliştiricileri, bu geliştirme ortamında C, C++, Python, Perl, Java ve C# gibi oldukça geniş bir dil ailesini kullanarak çalışabilirler. Bu sayede yüksek kaliteli yazılımlar geliştirilebilir, bu yazılımlar masaüstündeki diğer bileşenlerle uyum içinde çalışabilir.

GNOME dünyası, GNOME Türkiye üyeleri için hem iş, hem de eğlence ortamı. GNOME hakkında daha geniş bilgi için [GNOME web sayfasına](https://www.gnome.org) göz atabilir, GNOME Türkiye çalışmalarına katılabilirsiniz.