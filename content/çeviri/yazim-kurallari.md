---
title: "Yazım Kuralları"
date: 2018-05-29T21:04:08+03:00
---

Cümle kurarken bazı ekleri ve kelimeleri hatalı yazıyoruz. Bu durum, çevirilerin denetimini  zorlaştırıyor, denetimi yapan kişinin zamanını alıyor. Eğer gözden kaçarsa hoş durmuyor.

Aşağıda belirli eklere ilişkin temel kuralları bulacaksınız. Çeviri yaparken bu kuralları gözönünde bulunduralım.

Gerekiyorsa emin olamadığınız konularda mutlaka gnome-turk@gnome.org eposta listesine danışın.

1. Yer belirtmeyen, bağlaç olan “de”, “da” ekleri ayrı yazılır. Yer, konum ve tarih belirtirse bitişik yazılır.

Örnek:
Adam sen de!
Ne malda var, ne de canda var.
Ankara’dan abim gelmiş.
Gelsen de gelmesen de umurumda değil.
Telefonda kim var?
Sistemlerde Apache çalışıyor.
Gecenin üçünde aranır mı?
Sende kaç YTL var?

2. Bağlaç olan -ki ayrı yazılır.

Hem -ki, hem de -de, -da bağlaçları ayrı yazılır ve cümleden
çıkartıldığı zaman anlamı bozmaz, sadece bazı durumlarda cümlenin
anlamında daralma meydana getirir.

Örnek:

Bir yemek yapmış ki, yeme de yanında yat.

cümlesinde “-ki” bağlacı çıkarıldığında

Bir yemek yapmış, yeme de yanında yat.

biçimini alır ki, cümlenin anlaşılırlığında bozulma meydana gelmez. Ama cümledeki abartma anlamının yok olduğu kavranabiliir.

3. Sıfat ve zamir yapan “ki” eki bitişik yazılır.

Örnek:

Seninki can da benimki patlıcan mı?
Yanıbaşımdaki adamcağız bayılıverdi.
Önceki örnek pek güzelmiş.
Benim elmam kurtlu çıktı, seninki ne durumda?

4. Soru eki olan “mi” kendinden önce gelen kelimeden ayrı yazılır. Kendisinden sonra gelen ekler bu eke bitişik yazılır.

Okulu yine erkenden kırdın mı?
Senin canın can da bizimki patlıcan mı? (harika örnek!)
Aynı memleketteniz, değil mi?
Horulduyor, acaba uyumuş mudur?

5. Pekiştirme sıfatları bitişik yazılır.

Örnek:

Apaçık
kapkara
kupkuru
sipsivri
sapasağlam

6. “Ya da” her zaman ayrı, “veya” her zaman bitişik yazılır.

7. Birkaç, birçok, pek çok gibi kelimelerin yazımı:

Örnek:

Birkaç
Birçok
Pek çok 