---
title: "Çeviri Programları"
date: 2018-05-29T19:40:03+03:00
---

[Bu makale güncelliğini yitirmiştir. Makaleyi güncelleyerek GNOME’a katkıda bulunabilirsiniz.]

Linux’un mümkün olduğunca fazla kişiye ulaşması için yerelleştirme önemlidir. Bu yüzden linux için geliştirilmiş farklı platformlarda çalışan ve farklı özellikler, değişik kullanım seçenekleri sunan çeviri yazılımları mevcuttur. Bu belgede bu yazılımlardan en popüler olanlarını anlatılmaktadır.
        
## KBabel
### Genel Bilgiler
Kbabel bu belgenin yazıldığı tarihte Stanislav Visnovsky <visnovsky@kde.org> tarafından geliştirilen KDE temel alınarak yazılmış gelişmiş bir çeviri programıdır. İçerisinde kendisine ait sözlük ve katalog yöneticisi gibi modüller bulundurur. Belgede geçen diğer programlar gibi GNU GPL lisansı altında dağıtılmaktadır. Web sitesi <http://i18n.kde.org/tools/kbabel/> adresindedir.

![KBabel](kbabel.png "KBabel")

### Kurulumu
Kbabel hemen hemen tüm dağıtımlarda gelmektedir. Çoğu zaman kdesdk paketi içinde bulunur. Yaygın dağıtımlardan birini kullananlar yum, apt-get ve urpmi ile Kbabel’i gereksinimleri ile birlikte yükleyebilir.
Kbabel’i derlemek için kdelibs’in ve Qt’nin kütüphanelerinin ilgili sürümleri gerekir. Bu gereksinimler sağlandıktan sonra, Kbabel web sitesinden indirebileceğiniz kaynak kodları aşağıdaki komutlar ile derlenebilir:

```bash
tar xjvf kbabel-%surum.tar.bz2
cd kbabael-%surum
./configure && make
```
Ve root olarak:
```bash
make install
```

### Kullanım
Kbabel’i komut satırından ‘kbabel’ yazarak, ya da ilgili dağıtımın menüsünden çalıştırabilirsiniz. Kbabel ilk açıldığında tercihlerinizi belirtmek için bir pencere açacaktır. Bu pencereye adınızı, Türkçe harflerle adınızı, e-posta adresinizi yazın. Tam dil adına ‘Turkish’, dil koduna ‘tr_TR.UTF-8’ yazın. Dil posta listesine Gnome çevirileri için ‘gnome-turk@gnome.org‘ posta listesini, GMT bölümüne de Türkiye için +2 yazın. Tekil çoğul biçimleri sayısı Türkçe için birdir ve GNU çoğul biçin başlığı ‘nplurals=1; plural=0;’ şeklindedir. Gnome için gerekli diğer gerekli olan ayarlar şu şekildedir:

Kaydet:
Tanım: ‘Translation of @PACKAGE@ to @LANGUAGE@’
Kodlama -> Öntanımlı: ‘UTF-8’

Çeşitli:
Klavye hızlandırıcıları için işaretçi: ‘&’ yerin ‘_’

Bu ayarlar yapıldıktan sonra ‘Tamam’ düğmesine basıp çevirilere başlayabilirsiniz.

‘Dosya’ menüsünden ‘Aç’ ile çevireceğiniz .po ya da .pot dosyasını açabilirsiniz. PO dosyası yüklendikten sonra, Kbabel istatistiksel bilgileri durum çubuğunda gösterecektir. Sağdaki panelde Arama, PO içeriği, Kaynak, Etiketler ve Karakterler sekmeleri bulunur. Bunlardan Arama, PO Compedium adı verilen geçmiş çevirilerin bir dizinini içeren veritabanından aramayı sağlar. Türkçe için genel kullanılan bir compedium verisi yoktur. PO içeriği, po dosyasının içerini düzenli bir şekilde gösterir, önceki ve sonraki girişleri görmeniz için bu sekme rahatlık sağlar. Kaynak bölümünde, eğer tercihleriniz içerisinde ayarladıysanız, PO dosyasının bulunduğu klasöre göreceli olarak, PO dosyasında belirtilen yoldan kaynak kodu gösterilir. Bu çeviri yaparken kelimenin nerede kullanıldığını anlayabilmek için kolaylık sağlayan bir sekmedir. Etiketler bölümü, içinde xml ve html benzeri etiketler bulunduran girişlerde içiçe geçmiş etiketleri kolaylıkla takip etmenizi sağlar. Son olarak Karakterler sekmesi klavyenizde bulunmayan diğer dillere ya da betiklere ait karakterleri çeviri içine ekleyebilmenizi sağlar.

KBabel geniş bir tuşbağı varlığına sahiptir. Hemen tüm çeviri işlevleri için bir tuş bağı mevcuttur. Bunlardan en faydalıları bir sonraki ve bir önceki girişe gitmek için “Page Up/Page Down”, bulanık çeviriler arasında dolaşmak için “Ctrl+Page Up/Ctrl+Page Down”, tercümesiz girişler arasında dolaşmak için “Alt+Page Up/Alt+Page Down” ve bulanık imini kaldırmak için Ctrl+u tuşbağlarıdır. Diğer işlevler için tuş bağlarını menülerden ya da Kbabel’in detaylı yardım metninden öğrenebilirsiniz. Tuş bağlarını kullanmak özellikle uzun çevirilerde size çok büyük kolaylık sağlayacaktır.

Çeviriniz bittikten sonra Kaydet tuşuna bastığınızda Kbabel çeviriyi kontrollerini yaparak kaydedecek, eğer hata bulursa bunu size bildirecektir.

## GTranslator
### Genel Bilgiler
GTranslator Fatih Demir < kabalak@kabalak.net> ve Ross Golder < ross@kabalak.net> tarafından geliştirilmektedir. Sadece Gnome için geliştirilen tek çeviri programıdır. KBabel’deki compedium’a benzer UMTF adı verilen öğrenme tamponları içerir. Web sitesi < http://gtanslator.sourceforge.net/ > adresinde bulunmaktadır.

![GTranslator](gtranslator.png "GTranslator")

### Kurulumu
GTranslator da Kbabel gibi oldukça yaygındır ve bu yüzden çoğu dağıtım içinde gelmektedir. Gene yum, apt-get ve urpmi gibi araçlarla ‘gtranslator’ı yükleyebilirsiniz.

Kaynaktan kurulumunu da tar dosyalarını gtranslator web sitesinden edinerek KBabel’de olduğu gibi yapabilirsiniz. GTranslator kurulum için gtk2 ve glib2 kütüphanelerine ihtiyaç duyar.

### Kullanım
GTranslator’ı ilk açtığınızda sade bir arabirim ile karşılaşırsınız. Üstte farklı işlevleri olan düğmeler ile solda Po dosyasının bir dizininin tutulacağı panel yer alır. Ortada kalan geniş alan çeviri için ayrılmıştır ve üstünde çeviri açıklamalarının bulunduğu bir metin kutusu vardır.

GTranslator ilk açılışında PO başlığı için gerekli bilgileri size sormaz. Bu bilgileri ‘Düzenle’ menüsündeki ‘Tercihler’ kısmından yapmanız gerekir. Burada ‘PO Başlığı’ sekmesinde adınızı, e-posta adresinizi ve dil seçeneklerini ayarlamanız gerekmektedir. Dil seçeneklerinde Gnome için dil ‘Turkish’, dil kodu ‘tr’, dil grubunun e-posta adresi ‘ gnome-turk@gnome.org‘, karakter kümesi ‘UTF-8’ ve kodlama ‘8bit’ olmalıdır.

Bu ayarladan sonra çevirinize başlayabilirsiniz. Bir po dosyası açtığınızda po dosyasının içeriği solda listelenecektir. GTranslator tuşbağları yönünden Kbabel’e göre yeterli değildir. Girişler arasında dolaşmak için Ctrl+İleri/Ctrl+Geri tuşbağlarını kullanabilirsiniz.

GTranslator çoğul biçimleri desteklememektedir. Dolayısıyla GTranslator ile çeviri yapıldığında çoğul biçimler daha sonra el ile düzeltilmelidir. Aksi halde PO dosyaları yanlış oluşacak ve sorunlara sebep olacaktır. Bu yüzden GTranslator ile yapılan çevirilerde PO dosyasının çoğul biçimleri içerip içermediği özellikle kontrol edilmelidir.

## poEdit
### Genel Bilgiler
poEdit Vaclav Slavic < vaclav.slavik@matfyz.cz> tarafından geliştirilen çapraz platform bir çeviri aracıdır. wxWidgets (eski adıyla wxWindows) kütüphanelerini kullandığı için Windows, Linux ve diğer platformlarda grafik arabirimi değişmeden kullanılabilir. Bu özellikle Windows üzerinde çeviri yapanlar için büyük bir kolaylıktır. Web sitesi < http://poedit.sourceforge.net/ > adresinde bulunmaktadır.

![poEdit](poedit.png "poEdit")

### Kurulum
poEdit rpm’leri kendi web sitesinde bulunabilmektedir. Ancak kaynaktan kurulumu da gayet kolaydır. poEdit’i kurabilmek için wxWidgets kütüphanesinin kurulu olması gerekir. Bu kütüphaneye <http://www.wxwidgets.org > adresinden ulaşılabilir. wxWidgets kütüphanesi kurulduktan sonra poEdit’in sitesinde edinilen kaynak kodları bir dizine açılarak derlenebilir. Derleme için KBabel’deki yöntem aynen izlenebilir. poEdit’i Windows’ta çalıştırmak için önceden derlenmiş kurulum programı mevcuttur. wxWidgets kütüphanelerinin Windows sürümü yüklendikten sonra poEdit Windows altında da sorunsuz kurulabilir.

### Kullanım
poEdit kullanımı kolay çeviri programlarından birisidir. İlk açılışta ayarları yapmanız için sizi uyarır ve Tercihler penceresini açar. Burada ilk sekmede adınızı ve e-posta adresinizi girmeniz
gerekmektedir. poEdit GTranslator ve Kbabel’dekine benzer çeviri kütükleri tutar. Çeviri Hafızası denilen bu kütükler Türkçe genel kullanım için mevcut değildir ancak siz kendi kullanımınız için
bir kütük oluşturabilirsiniz.

Diğer ayarları da kendi tercihleriniz doğrultusunda yaptıktan sonra çeviri yapmaya başlayabilirsiniz. poEdit arabirimi iki alandan oluşur, üstte çeviri dosyasının dizini, altta ise çeviri yapacağınız alan bulunmaktadır.

poEdit PO dosyalarını bir katalog içinde ele alır. Katalog PO dosyasının oluşturulduğu ortamı belirtir. PO dosyaları kaynak kodlarından ayıklanarak oluşturulur. Bu sebeple poEdit Katalog menüsünde bu süreçle ilgili özelliklere sahiptir. Mevcut çevirinizi POT dosyası ile birleştirebilir, kaynak kodlarından baştan bir dosya oluşturabilirsiniz. Takım bilgileri, karakter kümesi gibi bilgileri de buradan girebilirsiniz. Katalog menüsünden Ayarları seçtiğinizde, poEdit size bu ayarları yapacağınız bir pencere açar. Bu pencerede gerekli olan bilgileri GTranslator ve Kbabel’deki gibi benzer şekilde doldurabilirsiniz.

poEdit’te dizgiler arasinda Ctrl+Yukarı/Ctrl+Aşağı tuşbağları ile gezinebilirsiniz. poEdit hatalı, eksik ya da bulanık çevirileri üst tarafta bulunan dizinda farklı renklerde gösterir. Aynı zamanda poEdit’te ‘Görünüm’ menüsünden ‘Satır Numaralarını Göster’ seçeneğini seçerek dizgilerin PO dosyası içindeki satır numaralarını görebilirsiniz. Bu bazen el ile düzenleme yapmanız gerektiğinde büyük kolaylık sağlar.