---
title: "Çeviri Nasıl Yapılır?"
date: 2018-05-29T19:36:34+03:00
---

Hazırlayan : Barış Çiçek

## Giriş
Bu belge Gnome Projesi yerelleştirme çalışmalarına katılabilmeniz için yardımcı bilgileri içerir. Bu belge sayesinde, yerelleştirme çalışmaları için temel bilgieri öğrenebilir, ve hatasız çeviriler yapabilmek için gerekli koşulları toplu bir halde bulabilirsiniz.

## PO dosyası

### Giriş
Gnome Projesi içerisindeki uygulamalar kaynak kodunda değişiklik yapmadan ve kolay bir şekilde çevirilebilmek için GNU kütüphanelerinden `gettext` kütüphanesini kullanır. Gettext kütüphanesi programcıların kod içerisinde çevirisi yapılması gereken metinleri işaretleyebilmelerini, ve bu işaretlenen metinlerin ayrı bir dosya içerisinde saklanabilmelerini sağlar. Bu kolaylık İngilizce ya da farklı dillerde yazılmış programların diğer dillere sadece bir dosyayı düzenleyerek çevirilebilmesini sağlar.

Gettext kullanan bir uygulama kaynak kodu içinde işaretlenen metinleri MO (makine nesnesi) dosyasından alır. MO dosyaları PO (uyarlanabilen nesne) dosyalarından derlenmiş ikili (binary) dosyalardır.

Düz metin halindeki PO dosyaları çeviricilerin herhangi bir metin düzenleyicisi ile daha sonra MO dosyasına derlenmek üzere oluşturabilecekleri dosyalardır.

### Yapısı

PO dosyaları işaretlenmiş metinlerin `xgettext` aracı ile ayıklanması yolu ile oluşturulabilen düz metin dosyalarıdır. Bir po dosyası içindeki girdinin (işaretlenmiş metinin) yapısı aşağıdaki gibidir:

 
```
boş satır

# çevirici için açıklamalar

#. otomatik oluşturulan açıklamalar

#: cevirinin kod içinde bulunduğu yerler…

#, imler…

msgid çevirilmemiş dizgi

msgstr çevirilmiş dizgi

```

Örnek:
```
#: lib/error.c:116

msgid “Unknown system error”

msgstr “Bilinmeyen sistem hatası”
```

### PO dosya imleri

PO dosyaları her girdi sonrası girdi ile alakalı olarak imler içerir. Bu imler çevirinin işlenmesi ile ilgili önemli bilgiler içerir. Gettext kütüphanesindeki belli başlı iki im ve açıklamaları şöyledir:

#### fuzzy (bulanık imi)

Bulanık imi çevirinin, çeviren tarafından tam olarak kestirilememesi, tereddütte kalınması halinde kullanılması gereken, ya da uygulanamanın içindeki metinlerin değişmesi sonucu araçlar tarafından otomatik olarak atan imdir. Bulanık imleri bulunan girdiler çeviri olarak gösterilmezler, ve çeviri istatistiklerinde bulanık olarak belirtilirler.

#### c-format ve no-c-format

Bu im genelde az rastlanılan ancak çeviri açısından önemli bir imdir. c-format, öntanımlı olarak tüm girdiler için geçerlidir. Metin’in c dizgi biçimi halinde olduğu anlamına gelir. C dizgi biçimi içerisinde dizgi için %s, tam sayı için %d, reel sayı için %f gibi kodlar bulunduran bir yapıdır. no-c-format içeren bir girdi bu kodların işleme koyulmayacağını yani %s kodunun % ve s karakterlerini belirttiğini anlatır. Genel olarak programlama_dili-format seklindedir.

İmler hakkında daha geniş bilgileri gettext belgelerinden edinebilirsiniz.
        
### Çoğul biçimleri

Gettext 0.10.36 sürümüyle birlikte, birden fazla çoğul hal içeren diller için “plural forms” eklenmiştir.

Türkçe’de sadece bir adet çoğul hal olması birden fazla çoğul hal çevirisi yapmayı gerektirmez, ancak biçim bütünlüğü için diğer çoğul hallerinde birinci ile aynı olması gerekir. Ancak bazen tekil halinde kod içerilmez. Bu durumda ayrı ayrı çeviriler belirtilmelidir. Benzer bir durum Türkçe için örnekteki gibidir. Çoğul biçim içeren PO girdisi aşağıdaki gibidir:

```
boş satır

# çevirici için açıklamalar

#. otomatik oluşturulan açıklamalar

#: cevirinin kod içinde bulunduğu yerler…

#, imler…msgid untranslated-string-singular

msgid_plural çevirilmemiş çoğul hal içeren dizgi

msgstr[0] ilk durum için çeviri

…

msgstr[N] n’nci durum için çeviri
```

Türkçe için örnek:
```
#: src/msgcmp.c:338 src/po-lex.c:699

#, c-format

msgid “found one fatal error”

msgid_plural “%d fatal errors”

msgstr[0] “Bir önemli hata bulundu”

msgstr[1] “%d önemli hata bulundu”
```

PO dosyasının yapısını bilmek çeviri için güzel bir arkaplan bilgisi oluşturur. Her ne kadar çeviri programları çeviriciler için sadece çeviriyi yazmaya kadar işi kolaylaştırsalarda bazen çeviri programını kullanmadan bir metin düzenleyici ile PO dosyasında değişiklik yapmak gerekebilir. Bunu doğru ve eksiksiz yapabilmek için yukarıda anlatılan bilgiler çoğu zaman olacaktır.

Her uygulama için PO dosyalarının çevirilmek için hazırlanmış POT (PO şablomu) dosyası vardır. POT dosyası yeni metinler için msgtr kısmı boş olan bir PO dosyasından ibarettir.

Türkçe’deki çoğul biçim PO dosya başlığı “Plural-Forms: nplurals=1; plural=0;\n” şeklindedir ve çoğul biçim bulunan PO dosyaları için PO başlığına eklenmesi gerekir. Bunu çeviri programınıza eklemek kolaylık getirecek, unutmanızı engelleyecektir. Çeviri proramlarında nasıl yapabileceğinizi öğrenmek için çeviri programları belgesini okuyabilirsiniz.

### Çeviri için POT dosyası edinmek
Çeviri yapabilmek için öncelikle çevirilecek uygulanaman POT dosyası elde edilmelidir. POT dosyası bir çok yardımcı uygulama ile uygulamanın kaynak kodlarından edinilebinir. Ancak gnome için standart kullanılan araclar `intltool` araçlarıdır.

#### Uygulama Kaynak Kodlarından
intltool araçları Gnome CVS ağacı altında ya da farklı dağıtımlar için paketlerini bulabileceğiniz bir perl betiğidir. (script) Çeviri için POT dosyası oluşturmak için öncelikle gnome cvs ağacından en son kaynak kodlarını çekmelisiniz. Bunun için paket_ismi yerine çevireceğiniz paketin ismini yazarak aşağıdaki komutları kullanabilirsiniz:

```bash
export CVSROOT=:pserver: anonymous@anoncvs.gnome.org:/cvs/gnome
cvs login
#şifre için Enter’a basın
cvs -z3 up paket_ismi
```

Kaynak kodlarını edindikten sonra paket\_ismi/po dizinine girmelisiniz. Burada `intltool-update -p` komutunu kullanarak paket\_ismi.pot dosyasını oluşturabilirsiniz. Eğer daha önceden bir çeviri var ise, mevcut çeviriyi en son pot dosyası ile birleştirmek, dolayısıyla sadece yeni dizgileri eklemek için `intltool-update tr` komutunu kullanabilirsiniz. Bu komut ile tr.po dosyası yeniden oluşacaktır.

#### Durum Sayfasından
Gnome Çeviri Takımı uygulama kaynak kodlarından POT dosyası edinmeyi kolaylaştırmak için durum sayfası üzerinde günlük olarak POT dosyalarını oluşturmaktadır. Bu sayede kaynak kodlarını indirmeden sadece POT dosyasını durum sayfasında paket isminin üzerine tıklayarak indirebilirsiniz. Durum sayfasına http://l10n.gnome.org/languages/tr adresinden ulaşabilirsiniz. Buradan yayınlanmış GNOME sürümlerine ait çevirilere ulaşabileceğiniz gibi güncel çevirileri de bulabilirsiniz. GNOME masaüstü ortamının son sürümünü için çeviri yapmaya başlamadan önce almak istediğiniz paket ile ilgili isteğinizi ve ilk defa GNOME çevirisi yapıyorsanız bunu da belirterek gnome-turk@gnome.org adresine bir e-posta atmanız hem sizi tanımamız hem de çevirilerin daha düzenli bir şekilde ilerlemesi için oldukça önemlidir.

### Çeviri Programları
PO dosyalarını düzenlemek için birçok program yazılmıştır bunlar arasından KDE için Kbabel, Gnome için Gtranslator ve Windows ortamı için POEdit en çok bilinenleridir. Bu programları aşağıdaki adreslerden indirebilirsiniz:

Kbabel: <http://i18n.kde.org/tools/kbabel/>

GTranslator: <http://gtranslator.sourceforge.net/>

PoEdit: <http://poedit.sourceforge.net/>

Programların kullanışları ile ilgili daha detaylı bilgiyi Çeviri Programları adlı belgede ya da programların kendi belgelerinde bulabilirsiniz.

## Çeviri yaparken dikkat edilecek noktalar

### Hatırlatıcı (mnemonic) İşareti
Hatırlatıcı Tuş uygulama arabirimlerinde gördüğünüz, altı çizgili harf ile belirtilen ve Meta tuşu ile kullanılan tuştur. Bu tuş belirtilirken GTK kütüphanesi karekterin önüne altçizgi(“_”) kullanımını gerektirir. Çeviri yaparken altçizgi çeviride de belirtilmelidir. Çeviri yaparken genellikle aynı karakteri kullanmak, eğer o karakterden çeviride bulunmuyorsa ilk harfin başına alt çizgi eklemek en doğrusudur. Bir dizgi içinde birden daha fazla alt çizgi bulunamaz.

Örnek:
```
#: src/main.c:284

msgid “_About”

msgstr “_Hakkında”
```

### Şemalar Dosyası Metinlerinin Çevirisi
Gnome uygulamaların yapılandırmalarını düzenlemek için Windows’taki Registry benzeri bir yapı kullanmaktadır. Bu yapıyı kullanabilmek için uygulamalar .schemas uzantılı ve yapılandırma ile ilgili bilgileri içeren bir XML dosyasını sisteme yüklerler. Bu dosyalar yapılandırma ile ilgili açıklamaları içerir. Bu dosyaların çevirilmesi konusunda dikkat etmek gereken en önemli husus, açıklamalarda anahtar değerlerini çevirmeden bırakmaktır.

Çünkü bu değerler dilden dile değişmez, ve orjinal halinde belirtildiğinin dışında kullanıldığında çalışmazlar. PO dosyasında bir girdinin şema dosyası olup olmadığını, metnin bulunduğu dosyanın adına bakarak anlayabilirsiniz. Eğer dosya .schemas dosyası ise burada genellikle tırnak içinde bulunan kelimeleri çevirmeden bırakmak gerekir.

Örnek:
```
#: applets/clock/clock.schemas.in.h:15

msgid “”

“This key specifies the format used by the clock applet when the format key “

“is set to \”custom\”. You can use conversion specifiers understood by “

“strftime() to obtain a specific format.”

msgstr “”

“Bu anahtar format anahtarı \”custom\”a ayarlı olduğunda saat uygulamacığında “

“kullanılacak olan biçimi belirtir. strftime() tarafından algılanabilen “

“biçim belirteçlerini bir biçim belirtmek için kullanabilirsiniz.”
```

### C-format Biçimi
Gettext kütüphanesi düz metinlerin çevirisine izin verdiği gibi c-format biçimindeki metinlerin de çevirilmesini sağlamıştır. c-format biçimi değişkenler içeren metin şablomlarıdır ve normal metinlerden farklı olarak sadece % ile başlayan kodlar içerirler. % ile başlayan kodların çeviri içinde de aynen bulunması zorunludur. Aşağıdaki bir kaç örnek kodların kullanımı ve yerlerini çeviri içinde düzenlemeyi göstermektedir:

```
msgid “%d folder and %d file have been deleted. (%s)”

msgstr “%d klasör ve %d dosya silindi. (%s)”
```

Bu örnekte %d ve %s kodları orjinal metinde kullanıldığı sırada kullanılmıştır.

```
mstid “%d folder and %d file have been deleted from folder %s”

mstid “%3$s klasöründen %2$d dosya ve %1$d klasör silindi”
```

Aşağıdaki örnek c-format biçiminde tarih ve zaman belirtmektedir. Burada Türkçe’deki tarih ve zaman yazımına dikkat edilerek çeviri yapılmalıdır.
```
msgid “Completed on %B %d, %Y, %l:%M %p”

msgstr “%d %b %l:%M %p tarihinde tamamlandı”
```

Bu örnekte kodların sıraları değiştirilmiştir. Sıra değiştirilirken önce % karakteri sonra kullanılacak kodun metin içindeki sırası daha sonra $ işareti ve kodun türü yazılır. %3$s kodu, orjinal metinde 3. sıradaki kod olan %s’yi belirtir.

no-c-format imi bulunan PO dosyası girdileri kodları derlemez o yüzden % karakterleri metnin bir parçası olarak görülmelidir. Eğer no-c-format flagı bulunmuyorsa yüzde işaretini belirtmek için iki kere yazmak gerekir.

Örnek:
```
#, c-format

msgid “%d% completed”

msgstr “%%%d tamamlandı”
```

### Python-format Biçimi
Bu biçim c-format biçimi için geçerli olan kuralları korur ancak python ve c programlama dillerini yapılarındaki değişikliklerden dolayı python-format biçiminde olan çevirilerde kullanılan kodlar farklıdır. python-format biçiminde ‘$’ işareti kodlamalarda kullanılmaz, onun dışında kullanım aynıdır.

Örnek:
```
#, python-format

mstid “%d folder and %d file have been deleted from folder %s”

mstid “%3s klasöründen %2d dosya ve %1d klasör silindi”

#, python-format

msgid “Completed on %B %d, %Y, %l:%M %p”

msgstr “%d %b %l:%M %p tarihinde tamamlandı”
```

### Yeni Satır Karakteri
Çeviriler içinde, özellikle konsola yazılacak olan metinler için satır sonlarında yeni satır karakteri (\n) bulunur. Bu karakterler çeviride de aynı miktarda bulunmalıdır. Orjinal metinin çevirisi daha kısa olsa bile boş satırlara \n eklenerek sayı tamamlanmalıdır. Ayrıca çevirdiğiniz metnin içinde görüneceği pencerenin boyutlarına uygun olması, metnin daha düzgün görüntülenmesini sağlayacağı için satır sonu karakteri ve çevirinin orjinal metin ile satır sonu olarak benzer olmasına dikkat etmeniz önerilmektedir.
        
### UTF-8 Kodlaması
Gnome çevirilerinde kullanılan PO dosyaları UTF-8 kodlanmalıdır. Çevirdiğiniz dosyanın UTF-8 kodlu olup olmadığını _file_ aracı ile kontrol edebilirsiniz. Eğer UTF-8 kodlu değil ise _iconv -from dosya_kodlaması -to utf-8_ komutu ile çevirimi yapabilirsiniz. Araçlar hakkında daha detaylı bilgiyi yardımlardan ya da kılavuz sayfalarından öğrenebilirsiniz.

### Son Kontrol
Çeviri bitip CVS’e gönderilmek üzere göndermeden önce dosya üzerinde son kontrolleri yapmalısınız. Bu kontrolleri _msgfmt_ adlı araç ile yapmanız mümkün. Dosyayı göndermeden önce aşağıdaki komutun hata belirtmediğinden emin olun:

`msgfmt -c -v –check-accelerators=_`

## Gnome-Turk Listesi
Çeviri yapmak GNOME hakkında bir çok şeyi öğrenmenizi sağlayacak ve Özgür Yazılım için katkıda bulunmanıza olanak sağlayacaktır. Çeviri yaparken burada yazanların dışında yardıma ihtiyacınız olabilir. Bu yüzden çeviri yapmadan önce Gnome-Turk listesine üye olmanız işinizi kolaylaştıracaktır. Bu sayede diğer çeviri yapanlardan yardım alabilir, etkinlikleri daha kolay takip edebilirsiniz. Gnome listesine üye olmak için <http://mail.gnome.org/mailman/listinfo/gnome-turk> adresine gidebilirsiniz.
