---
title: "Çeviri Deposu İş Akışı"
date: 2018-05-29T20:04:55+03:00
---

## GİRİŞ
Bu belge GNOME Türkiye çevirileri için iş akışının nasıl olduğunu göstermektedir. GNOME Türkiye çevirilerinin gönderimi ve kontrolü için GNOME Translation Project (GTP) tarafından kullanılan Damend Lies istatistik aracını kullanmaktadır. Damned Lies çeviri durumlarını göstermek ile birlikte çevirilerin saklanması, kontrol edilmesi gibi iş akışını GNOME çeviri takımları için kolaylaştıran bir akıştır.

Bu belgede GNOME çevirilerine katılacak olanlar için bu iş akışının nasıl gerçekleştigi ve  çeviriler hakkında temel bilgiler bulunmaktadır.

## GNOME Çeviri Sistemi
GNOME Projesi uygulamaların yerelleştirmesi için geniş olanaklar sağlayan Gettext sistemini kullanmaktadır. Bu sistem sayesinde uygulamalar sistemin kendi dil ayarlarına göre uygun tercümeler ile görüntülenir. Gettext sistemi uygulamaların kaynak kodları içerisinde çevirisi yapılması gereken dizgileri ayrı bir dosyada (PO dosyası) içerisinde saklar ve bu sayede uygulamaların kurulumları sırasında farklı dillerin uygulama tarafından desteklenmesine olanak sağlar. Bu sistem aynı zamanda belgelendirmelerin çevirilerinde de kullanılmaktadır.

GNOME Çevirileri uygulamaların şablom PO dosyaları üzerinden yapılmaktadır. PO dosyalarının düzenlemek için bir çok uygulama mevcuttur ve dosyaların belirli sözdizimi özelliklerine sadık kalınarak bu değişiklikler yardımcı programların sayesinde kolaylıkla yapılmaktadır. Bu dosyaların nasıl çevirileceğinin detaylı bir anlatımını Çeviri Nasıl Yapılır? belgesinde bulabilirsiniz.

## GNOME Türkiye Çevirileri İş Akışı
GNOME Çevirileri dağıtımların ve uygulamaların paketlerine dahil edilebilmeleri için GNOME Projesi sürüm kontrol sistemine eklenilmektedir. Bu sistem erişim kontrolu içerdiği için, bu işlem çeviri kordinatörleri tarafından gerçekleştirilmektedir. GNOME Türkçe çevirileri için çeviri kordinatörü Türkçe Takımı
sayfasında görülebilir.

Çevirilerin Dizgi Donması sürecinde düzenli bir şekilde çevirenler tarafından paketlerin alınması, kontrol edilmesi ve sürüm kontrol sistemine gönderilmesi için Damned Lies tarafından sağlanan takip sistemi kullanılmaktadır.

Bu sistem çevirilen paketlerin belirlenmesi, çeviri yapanların yaptıkları çevirileri kontrol için gönderilmesi ve bu kontrollerin yapıldığı süreci kendilerinin de takip edebilmelerini sağlamaktadır. Belirli bir iş akışı içerisinde gerçekleşen bu süreç genel olarak aşağıdaki gibi işlemektedir:
 
### Çeviricilerin Sisteme Kayıt Olması
Çeviriciler, çeviri çalışmalarına katılmak çeviriciler için öncelikle Damned Lies sitesine ve GNOME Türkiye Posta Listesi’ne kayıt olmak zorundadırlar. Kayıt olmadan çeviri gönderemezler.

Posta listesine kayıt olmak için kayıt ekranındaki bilgileri doldurmanız yeterlidir. Gnome-turk listesi düşük trafikli bir posta listesidir. Çeviri dönemlerini takip etmeniz, diğer çeviricilerle ve GNOME Turkiye üyeleri ile iletişim kurmanız açısında posta listemize kayıt olmanız önemlidir. Kayıt olduktan sonra ufak bir tanıtıcı mektup ile ‘Merhaba’ diyebilirsiniz.

Damned Lies sistemine kayıt olmak için öncelikle Kayıt sayfasından kullanıcı adı ve parolanızı belirlemeniz daha sonra e-posta adresinize gelecek olan etkinleştirme mektubundaki bağı ziyaret etmeniz gerekmektedir.

Hesabınızı etkinleştirdikten sonra belirlediğiniz kullanıcı adı ve parola ile siteye sağ üst kısımda bulunan ‘Login’ bağı ile giriş yapabilirsiniz.

Giriş yaptıktan sonra gene sağ üst köşedeki ‘Change your details’ bölümünü seçerek profil bilgilerinizi güncellemeniz gerekmektedir.

Bu bölümde öncelikle First name (İsminiz) ve Last name (Soy isminiz) bölümlerini doldurun. Diğer alanları da tercihinize göre doldurabilirsiniz. Bu bilgileri kayıt ettikten sonra. Gene aynı sayfada bulunan ‘Join a team’ bağını kullanarak çeviri yapacağınız dili ya da dilleri seçin. Turkçe için ‘Turkish’ seçip ‘Join’ düğmesini kullanabilirsiniz.

### Çevirilerin Seçilmesi ve Gönderilmesi
Kayıt işlemi bittikten sonra çevirisini yapacağınız paketleri seçip çeviriye başlayabilirsiniz. Paket seçimi için Gnome-turk posta listesine başvurmanız önemlidir. Çünkü bazı paketler uzun dönemdir belirli çeviriciler tarafından çevirilmektedir ve çeviri kalitesini arttıran bu süreklilik yüzünden bazı paketlerin çevirisi gene aynı çeviriciler tarafından üstlenilmek istenebilir. Bunu gnome-turk posta listesine alacağınız paketi belirttiğiniz zaman öğrenebilirsiz.

Alacağınız paket için bir engel bulunmuyorsa  Damned Lies sitesinde ‘Release sets‘ bölümünden dil olarak ‘Turkish’i seçtikten sonra en son sürümü seçin. Yeni sürümler eski sürümlerdeki çevirilerin yerine geçeceği için eski sürümleri çevirmek bir pek bir anlam ifade etmemektedir. (Ancak kararlı ara sürümler için örneğin GNOME 2.24.1 bu gerekli olabilir. Çünkü bu hata giderme sürümlerindeki çeviriler dağıtımlar tarafından paketlenmektedir.) Dizgi donması sürecine girildiğinde en sonra sürümü çevirmeniz esastır.

En yeni sürüm listelenenler arasında en üst sırada ve sürüm numarası en yüksek olandır. Hangi sürümü çevireceğiniz hakkında tereddütte kaldığınızda bilgi almak için gnome-turk posta listesine başvurabilirsiniz.

Sürüm seçimini yaptıktan sonra paket seçimine geçebilirsiniz. Bu noktada yukarıda belirtildiği gibi gnome-turk posta listesine danışın. GNOME Projesi içerisinde GNOME sürüm sürecine dahil olan ve olmayan paketler bulunmaktadır. GNOME sürüm sürecine dahil olmayan projeler ‘Extras’ adı altında yer alır. Çeviri önceliğimiz GNOME sürüm sürecine dahil olan paketlerdedir, ancak bazı yaygın kullanılan paketlerin (örneğin GIMP, gnumeric, gthumb) çevirileri de önemlidir. Fakat bu paketlerin dizgi donma süreçleri belirsiz olduğu için bu paketlerin seçiminin takibini çeviricinin kendisi yapmalıdır. Bu paketlere Damned Lies ana sayfasındaki ‘Modules‘ bölümünden ulaşabilirsiniz.

Çevirmek istediğiniz paketlin bağını tıkladıktan sonra çeviri için seçim yapabilirsiniz.

Paketi çeviri için ayıran olmadı ise paketin sayfasında bulunan ‘New Action’ bölümünde ‘Reserve for translation’ seçeneğini seçerek paketin artık sizin tarafınızdan çevirileceğini belirtebilirsiniz. Burada ‘Comment’ bölümüne çeviriyi ne kadar sürede tamamlayabileceğinize dair tahmini bir süre belirtin.

Yaptığınız paket seçimlerini toplu olarak gnome-turk listesine belirtmeyi unutmayın.

Çeviriyi tamamladıktan sonra gene aynı sayfaya gelerek ‘Upload the new translation’ bölümünü seçin. ‘Comment’ bölümüne tereddütte kaldığınız dizgileri ya da belirteceğiniz başka bilgileri ekleyebilirsiniz. Cevirisini tamamladığınız PO dosyasını da belirterek dosyayı gönderin.

 
### Çeviri Dosyalarının Takibi
Çevirini gönderdiğiniz ve üzerinizde bulunan paketleri profil sayfanızda görebilirsiniz. Bu sayfada gönderdiğiniz çevirilerin henüz kontrol edilip edilmediğini de takip edebilirsiniz.

Çeviri kordinatörü çeviriyi sürüm kontrol sistemine gönderdikten sonra paket profilinizden silinecektir. Bu işlem alakalı bilgiyi gnome-turk listesine belirli aralıklarla çeviri kordinatörü tarafından gelen mektuplardan da takip edebilirsiniz.