---
title: "İndir"
date: 2018-06-14T01:50:02+03:00
---

GNOME 3, GNU/Linux dağıtımlarının çoğu üzerinden kurulabilir. Ayrıca bunlardan birçoğu, kurulumdan önce GNOME masaüstü ortamını denemenize olanak tanır.

## [Fedora](https://getfedora.org/tr/)
Fedora, doğrudan GNOME3 ile gelir; Fedora’yı diskinize kurabilir ya da herhangi bir kurulum yapmadan canlı kalıbı kullanarak deneyebilirsiniz. Fedora 22, GNOME 3.16 ile birlikte gelmektedir.
